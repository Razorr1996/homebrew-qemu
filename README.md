# Abasalaev Qemu

## How do I install these formulae?

`brew install abasalaev/qemu/<formula>`

Or `brew tap abasalaev/qemu` and then `brew install <formula>`.

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
